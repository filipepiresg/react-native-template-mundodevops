module.exports = {
  root: true,
  env: {
    es6: true,
    jest: true,
    browser: true,
  },
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
    __DEV__: true,
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  extends: ['@react-native-community', 'airbnb', 'prettier', 'prettier/react'],
  plugins: ['@react-native-community', 'react', 'jsx-a11y', 'import', 'react-hooks', 'prettier'],
  parser: 'babel-eslint',
  rules: {
    camelcase: 'off',
    'prettier/prettier': ['error'],
    'react/destructuring-assignment': [0],
    'react/prop-types': [0],
    'react/state-in-constructor': 'off',
    'react/static-property-placement': 'off',
    'react/jsx-filename-extension': [1, { extensions: ['.js', '.jsx'] }],
    'react/jsx-props-no-spreading': [0],
    'no-return-assign': 'off',
    'no-underscore-dangle': 'off',
    'import/prefer-default-export': 'off',
    'no-unused-vars': ['error', { argsIgnorePattern: '^_' }],
    'react/jsx-one-expression-per-line': 'off',
    'global-require': 'off',
    'react-native/no-raw-text': 'off',
    'no-console': ['error', { allow: ['tron'] }],
    'react-hooks/rules-of-hooks': 'error',
    'react-hooks/exhaustive-deps': 'warn',
    'no-param-reassign': 'warn',
  },
  settings: {
    'import/resolver': {
      'babel-plugin-root-import': {
        rootPathSuffix: 'src',
      },
    },
  },
};
