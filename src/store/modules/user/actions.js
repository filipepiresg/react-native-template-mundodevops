import * as UserTypes from './types';

export function getProfileRequest() {
  return {
    type: UserTypes.GET_PROFILE_REQUEST,
  };
}

export function getProfileSuccess(data) {
  return {
    type: UserTypes.GET_PROFILE_SUCCESS,
    payload: { data },
  };
}

export function getProfileFailure() {
  return {
    type: UserTypes.GET_PROFILE_FAILURE,
  };
}
