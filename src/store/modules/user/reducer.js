import produce from 'immer';

import * as UserTypes from './types';

const INITIAL_STATE = {
  profile: null,
  medical_records: [],
};

function app(state = INITIAL_STATE, action) {
  return produce(state, (draft) => {
    switch (action.type) {
      case UserTypes.GET_PROFILE_SUCCESS: {
        draft.profile = { ...draft.profile, ...action.payload.data };
        break;
      }
      default:
    }
  });
}

export default app;
