import { Alert } from 'react-native';
import { takeLatest, call, put, all, select } from 'redux-saga/effects';

import api from '~/services/api';

import { getProfileSuccess, getProfileFailure } from './actions';
import * as UserTypes from './types';

export function* getProfile() {
  try {
    const { id: userId } = yield select((state) => state.user.profile);
    const endpoint = `users/${userId}/`;

    const response = yield call(api.get, endpoint);

    yield put(getProfileSuccess(response.data));
  } catch (err) {
    if (__DEV__) console.tron.log('getProfile - err', err);
    Alert.alert('Erro!', 'Solicitacao falhou.');

    yield put(getProfileFailure());
  }
}

export default all([takeLatest(UserTypes.GET_PROFILE_REQUEST, getProfile)]);
