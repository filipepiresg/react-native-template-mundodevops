import * as AppTpes from './types';

export function startRequest() {
  return {
    type: AppTpes.START_REQUEST,
  };
}

export function stopRequest() {
  return {
    type: AppTpes.STOP_REQUEST,
  };
}
