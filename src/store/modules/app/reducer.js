import produce from 'immer';

import * as AppTypes from './types';

const INITIAL_STATE = {
  loading: false,
};

function app(state = INITIAL_STATE, action) {
  return produce(state, (draft) => {
    switch (action.type) {
      case AppTypes.START_REQUEST: {
        draft.loading = true;
        break;
      }
      case AppTypes.STOP_REQUEST: {
        draft.loading = false;
        break;
      }
      default:
    }
  });
}

export default app;
