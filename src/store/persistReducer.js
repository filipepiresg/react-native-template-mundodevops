import { persistReducer } from 'redux-persist';
import AsyncStorage from '@react-native-community/async-storage';
import { Constants } from '~/commom';

export default (reducers) => {
  const persistedReducer = persistReducer(
    {
      key: Constants.PREFIX,
      storage: AsyncStorage,
      whitelist: ['user'],
    },
    reducers
  );

  return persistedReducer;
};
