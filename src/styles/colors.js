export default {
  WHITE: '#fff',
  BLACK: '#000',
  PLACEHOLDER: '#a0a0a0',
  RED: '#f00',
  TRANSPARENT: 'transparent',
};
