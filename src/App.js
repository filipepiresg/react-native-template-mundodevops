import React from 'react';

import { AppNavigator, NavigationService } from './routes';

function App() {
  return (
    <>
      <AppNavigator
        ref={(navigatorRef) => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}
      />
    </>
  );
}

export default App;
