import { NavigationActions, StackActions } from 'react-navigation';

let _navigator;

function setTopLevelNavigator(navigatorRef) {
  _navigator = navigatorRef;
}

function navigate(routeName, params) {
  _navigator.dispatch(
    NavigationActions.navigate({
      routeName,
      params,
    })
  );
}

function goBack(options = {}) {
  _navigator.dispatch(NavigationActions.back(options));
}

function replace(routeName) {
  _navigator.dispatch(
    StackActions.replace({
      routeName,
    })
  );
}

const NavigationService = {
  setTopLevelNavigator,
  navigate,
  goBack,
  replace,
};
// add other navigation functions that you need and export them
export default NavigationService;
