import { createStackNavigator } from 'react-navigation-stack';

import { HomeScreen } from '~/screens';

const MainNavigator = createStackNavigator(
  {
    Main: {
      screen: HomeScreen,
    },
  },
  {
    initialRouteName: 'Main',
    headerMode: 'none',
  }
);

export default MainNavigator;
