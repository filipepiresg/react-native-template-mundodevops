import { createAppContainer, createSwitchNavigator } from 'react-navigation';

import MainNavigator from './Navigation/MainNavigator';

const Navigator = createSwitchNavigator({
  MainNavigator,
});

const AppNavigator = createAppContainer(Navigator);

export default AppNavigator;
