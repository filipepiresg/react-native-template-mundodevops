import { format, parse } from 'date-fns';

import i18n from '~/i18n';

/**
 * Removes mask from given cpf
 *
 * @param {string} cpf masked cpf
 * @returns {string} unmasked cpf
 * @example
 * unmaskCPF('123.456.789.00')
 * '12345678900'
 */
export function unmaskCPF(cpf) {
  return cpf.replace(/\D/g, '');
}

export function maskCPF(cpf) {
  return cpf.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/, '$1.$2.$3-$4');
}

/**
 * Parses date string from pt-BR format to database format
 *
 * @param {string} date pt-BR date
 * @param {string} from current format
 * @param {string} to expected format
 * @example
 * formatDate('20/12/2019')
 * '2019-12-20'
 */
export function formatDate(date, from = 'dd/MM/yyyy', to = 'yyyy-MM-dd') {
  return format(parse(date, from, new Date()), to);
}

/**
 * Formats number to currency format
 *
 * @param {number | string} value value to be formatted
 * @example
 * toCurrency(15)
 * 'R$ 15,00'
 */
export function toCurrency(value) {
  return i18n.toCurrency(Number(value), {
    precision: 2,
    delimiter: '.',
    separator: ',',
    unit: 'R$ ',
  });
}

/**
 * Capitalize the first letter of a given string
 *
 * @param {string} word Word to be capitalized
 * @example
 * capitalize(mastercard)
 * 'Mastercard'
 */
export function capitalize(word) {
  if (typeof word !== 'string') {
    return '';
  }

  return word.charAt(0).toUpperCase() + word.slice(1);
}

/**
 * Obfuscate credit card number to hide sensitive data
 *
 * @param {string} lastDigits last digits from credit card number
 * @example
 * obfuscateCreditCard('1234')
 * '**** **** **** 1234'
 */
export function obfuscateCreditCard(lastDigits) {
  if (!lastDigits) {
    return '';
  }

  return `**** **** **** ${lastDigits}`;
}

/**
 * Format phone number to remove any character that is not a word character
 *
 * @param {string} phone phone number
 * @returns {string} phone formatted
 * @example
 * formatPhone('+55 11 912345678')
 * '(11) 91234-5678'
 */
export function formatPhone(phone) {
  if (!phone) {
    return '';
  }
  const phoneFormated = String(phone).replace('+55', '').replace(/\W/g, '');

  if (phone.length < 10) {
    const first_digits = phoneFormated.slice(0, -4);
    const last_digits = phoneFormated.slice(-4);
    return `${first_digits}-${last_digits}`;
  }
  const ddd = phoneFormated.slice(0, 2);
  const first_digits = phoneFormated.slice(2, -4);
  const last_digits = phoneFormated.slice(-4);
  return `(${ddd}) ${first_digits}-${last_digits}`;
}
