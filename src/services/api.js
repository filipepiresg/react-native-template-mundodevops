import axios from 'axios';

export const Endpoints = {
  // BaseURL: '',
};

const api = axios.create({
  baseURL: Endpoints.BaseURL,
  timeout: 7000,
  headers: {
    'Content-Type': 'application/json',
    Accept: 'application/json',
  },
});

export default api;
