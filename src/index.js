import React from 'react';
import { Platform } from 'react-native';
import CodePush from 'react-native-code-push';
import KeyboardManager from 'react-native-keyboard-manager';
import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';

// This line must come before the redux store import
import 'react-native-gesture-handler';
import './config/Reactotron';

import RootApp from './App';
import { store, persistor } from './store';

if (Platform.OS === 'ios') {
  KeyboardManager.setToolbarPreviousNextButtonEnable(true);
}

function Index() {
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <RootApp />
      </PersistGate>
    </Provider>
  );
}

export default CodePush({
  checkFrequency: CodePush.CheckFrequency.ON_APP_START,
  installMode: CodePush.InstallMode.ON_NEXT_RESTART,
})(Index);
