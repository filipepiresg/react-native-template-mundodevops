import React from 'react';

import { Container, Content, Text, StatusBar } from './styles';

function HomeScreen() {
  return (
    <Container>
      <StatusBar />
      <Content>
        <Text>
          Edit <Text hasBackground> src/screens/HomeScreen/index.js </Text> to change this screen
        </Text>
      </Content>
    </Container>
  );
}

export default HomeScreen;
