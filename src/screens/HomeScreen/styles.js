import styled from 'styled-components/native';

import { colors, metrics, fonts } from '~/styles';

export const StatusBar = styled.StatusBar.attrs({
  animated: false,
  backgroundColor: colors.WHITE,
  barStyle: 'dark-content',
})``;

export const Container = styled.SafeAreaView`
  flex: 1;
  background-color: ${colors.WHITE};
  padding: ${metrics.SCREEN_WIDTH * 0.2}px;
`;

export const Content = styled.View`
  flex: 1;
  align-items: center;
  justify-content: center;
`;

export const Text = styled.Text`
  font-size: ${fonts.MEDIUM}px;
  color: ${(props) => (props.hasBackground ? colors.WHITE : colors.BLACK)};
  background-color: ${(props) => (props.hasBackground ? colors.PLACEHOLDER : colors.TRANSPARENT)};
  font-family: Helvetica;
  text-align: center;
`;
