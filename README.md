# React Native Template MundoDevOps

## Intro

Esse projeto foi feito para auxiliar no desenvolvimento de apps da Mundo DevOps.

## Usage

```
npx react-native init <ProjectName> --template mundodevops
```

## Libraries

Temos instalados as seguintes libs:

- @react-native-community/async-storage
- appcenter
- appcenter-analytics
- appcenter-crashes
- axios
- date-fns
- formik
- i18n-js
- immer
- lodash
- lottie-ios
- lottie-react-native
- react-native-code-push
- react-native-gesture-handler
- react-native-keyboard-manager
- react-native-linear-gradient
- react-native-localize
- react-native-reanimated
- react-native-snap-carousel
- react-native-splash-screen
- react-native-vector-icons
- react-navigation
- react-redux
- redux
- redux-persist
- redux-saga
- styled-components
- validator
- yup

## What do you need to do?

É necessário a configuração do [code-push](https://microsoft.github.io/code-push) (`no arquivo src/index.js e nativamente`), [splash-screen](https://github.com/crazycodeboy/react-native-splash-screen#readme) (nativamente, seguindo o link anterior)
